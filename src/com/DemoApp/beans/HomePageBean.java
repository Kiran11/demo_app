package com.DemoApp.beans;

import org.openqa.selenium.By;

public class HomePageBean {

	private By button1 = By.xpath("//*[contains(@text,'BUTTON CLICK 1')]");
	private By button2 = By.xpath("//*[contains(@text,'BUTTON CLICK 2')]");
	private By alert = By.id("android:id/alertTitle");
	private By message = By.id("android:id/message");
	private By ok = By.id("android:id/button1");
	private By menu = By.className("android.widget.Button");
	private By homeButton = By.xpath("//*[contains(@text,'Home')]");
	private By ionicMenuStarter = By.xpath("//*[contains(@text,'Ionic Menu Starter')]");
	
	
	public By getIonicMenuStarter() {
		return ionicMenuStarter;
	}
	public void setIonicMenuStarter(By ionicMenuStarter) {
		this.ionicMenuStarter = ionicMenuStarter;
	}
	public By getHomeButton() {
		return homeButton;
	}
	public void setHomeButton(By homeButton) {
		this.homeButton = homeButton;
	}
	public By getMenu() {
		return menu;
	}
	public void setMenu(By menu) {
		this.menu = menu;
	}
	public By getButton1() {
		return button1;
	}
	public By getAlert() {
		return alert;
	}
	public void setAlert(By alert) {
		this.alert = alert;
	}
	public By getMessage() {
		return message;
	}
	public void setMessage(By message) {
		this.message = message;
	}
	public By getOk() {
		return ok;
	}
	public void setOk(By ok) {
		this.ok = ok;
	}
	public void setButton1(By button1) {
		this.button1 = button1;
	}
	public By getButton2() {
		return button2;
	}
	public void setButton2(By button2) {
		this.button2 = button2;
	}

}
