package com.DemoApp.pageobjects;


import java.util.List;

import com.DemoApp.beans.HomePageBean;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

/**
 * 
 * This class is used to get the components of home page
 */
public class HomePage {

	public AndroidDriver<AndroidElement> driver;

	HomePageBean home = new HomePageBean();

	public HomePage(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}
	public boolean button1click() {
		try {
			AndroidElement button = driver.findElement(home.getButton1());
			button.click();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean button2click() {
		try {
			AndroidElement button = driver.findElement(home.getButton2());
			button.click();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public String AlertTitle() {
		try {
			AndroidElement alertTitleText = driver.findElement(home.getAlert());
			return alertTitleText.getText();
		} catch (Exception e) {
			return "";
		}
	}

	public String AlertMessage() {
		try {
			AndroidElement alertMessage = driver.findElement(home.getMessage());
			return alertMessage.getText();
		} catch (Exception e) {
			return "";
		}
	}
	public boolean OKclick() {
		try {
			AndroidElement button = driver.findElement(home.getOk());
			button.click();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	public boolean MenuClick() {
		try {
			List<AndroidElement> elements = driver.findElements(home.getMenu());
			AndroidElement menuButton = elements.get(0);
			menuButton.click();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	public boolean SelectHome() {
		try {
			AndroidElement button = driver.findElement(home.getHomeButton());
			button.click();
			Thread.sleep(2000);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	public String IonicMenuStarter() {
		try {
			AndroidElement ionicMenuStartertext = driver.findElement(home.getIonicMenuStarter());
			return ionicMenuStartertext.getText();
		} catch (Exception e) {
			return "";
		}
	}

}
