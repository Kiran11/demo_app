package com.DemoApp.baseclass;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
/**
 * This class is base class for connecting the device into and the appium server
 * Once the this class gets executed successfully it launches the application.
 * 
 */
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileCapabilityType;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;



public class BaseClass {

	public static AndroidDriver<AndroidElement> driver;


	@Test
	public static void LaunchApp() throws MalformedURLException, InterruptedException {

		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("platformVersion", "6.0");
		capabilities.setCapability("deviceName", "3b7ea939");
		capabilities.setCapability("appPackage", "io.ionic.starter");
		capabilities.setCapability("appActivity", "io.ionic.starter.MainActivity");

		capabilities.setCapability(MobileCapabilityType.NO_RESET, "true");  
		driver = new AndroidDriver<AndroidElement>(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		Thread.sleep(2000);	
		
	/*	List<AndroidElement> elements = driver.findElementsByClassName("android.widget.Button");
		 AndroidElement ele = elements.get(0);
		 ele.click();
		System.out.println("sdf");*/
	}
}