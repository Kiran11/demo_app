/**
 * 
 * @author Ujjwal Abhishek
 * This class is used to automate the Login functionality of the DemoApp
 */
package com.DemoApp.testscripts;


import org.testng.annotations.Test;

import com.DemoApp.baseclass.BaseClass;

import com.DemoApp.pageobjects.HomePage;



public class HomePageTest extends BaseClass {


	HomePage homepage;


	@Test()
	public void homePageCheck() throws Exception 
	{
		System.out.println("Inisde homepage test");

		homepage = new HomePage(driver);   
		if(homepage.button1click())
		{
			System.out.println("button 1 clicked");	
			if(homepage.AlertTitle().equalsIgnoreCase("Alert") && homepage.AlertMessage().equalsIgnoreCase("Button Click 1"))
			{
				System.out.println("Correct alert popup is displayed");
				if(homepage.OKclick())
				{
					System.out.println("ok button has been clicked");
				}
				else
				{
					System.out.println("ok button not clicked");
				}
			}
			else
			{
				System.out.println("Correct alert pop was not displayed");
			}
		}
		else
		{
			System.out.println("button not clicked");
		}

		if(homepage.button2click())
		{
			System.out.println("button 2 clicked");	
			if(homepage.AlertTitle().equalsIgnoreCase("Alert") && homepage.AlertMessage().equalsIgnoreCase("Button Click 2"))
			{
				System.out.println("Correct alert popup is displayed");
				if(homepage.OKclick())
				{
					System.out.println("ok button has been clicked");
				}
				else
				{
					System.out.println("ok button not clicked");
				}
			}
			else
			{
				System.out.println("Correct alert pop was not displayed");
			}
		}
		else
		{
			System.out.println("button not clicked");
		}
		if(homepage.MenuClick())
		{
			System.out.println("Menu button has been clicked");
			if(homepage.SelectHome())
			{
				System.out.println("Home option has been selected");
				if(homepage.IonicMenuStarter().equalsIgnoreCase("Ionic Menu Starter"))
				{
					System.out.println("Home screen is displayed");
				}
				else
				{
					System.out.println("Home screen is not displayed");
				}			
			}
			else
			{
				System.out.println("Home option could not be selected");
			}
		}
		else
		{
			System.out.println("Menu button could not be clicked");
		}	
	}  
}
